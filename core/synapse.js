/**
 * Created by root on 02.11.15.
 */

var synaps = function(emitter,collector){

    this.emitter = emitter;
    this.collector = collector;
    emitter.outputs.push(this);
    collector.inputs.push(this);
    this.weigth = 0;

}

//invoked by emitter
synaps.prototype.emit = function()
{
    this.collector.collect(this.emitter.signal * this.weigth);
}

synaps.prototype.modifyWeight = function(delta)
{
    this.weigth+=delta;
}

module.exports = synaps;