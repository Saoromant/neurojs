/**
 * Created by root on 02.11.15.
 */
var layer = require('./layer.js');
var neuron = require('./neuron.js');
var net = function () {
    this.interpreter = new layer(0); //interpreter layer, must be learned 
    this.input =  new layer(300*300); //receptors layer
    this.input.connectNext(this.interpreter);
}

net.prototype.learn= function(input,associatedValue) {
    var resNeuron;
    for(var i = 0;i < this.interpreter.neurons.length;i++){
        var n = this.interpreter.neurons[i];
        if(n.associatedValue == associatedValue){
            resNeuron = n;
            break;
        }
    }
    if(!resNeuron){
        resNeuron = new neuron(this.input,this.interpreter,false,associatedValue);
    }
    var sum = 0;
    for(var i = 0;i<input.length;i++){
        var inSig = input[i];
        resNeuron.inputs[i].modifyWeight(inSig);
        sum += inSig;
    }
    resNeuron.setActivationLevel(1);
}

net.prototype.test = function(input){

    for(var i = 0;i<input.length;i++){
       var sig = input[i];
        var rec = this.input.neurons[i]
        rec.collect(sig);
    }
    var out = this.interpreter.neurons[0];
    if(!out)
    return 'nothing here';
    for(var i = 1;i<this.interpreter.neurons.length;i++){

        var n =this.interpreter.neurons[i];

        if(n.signal>out.signal)
            out = n;
    }
var s = out.signal
    this.interpreter.reset();
    this.input.reset();
    return out.associatedValue;
}

module.exports = net;