/**
 * Created by root on 02.11.15.
 */

var neuron = require('./neuron.js')
var synapse = require('./synapse.js')
var layer = function (nNumber) {
   this.neurons = [];

    for(var i =0; i<nNumber;i++)
    {
        this.neurons.push(new neuron());
    }
}

layer.prototype.connectNext = function (nextLayer) {
    this.nextLayer = nextLayer;
    for(var i = 0;i<this.neurons.length;i++)
    {
        var n = this.neurons[i];
        for(var j =0;j<nextLayer.neurons.length;j++)
        {
            nn = nextLayer.neurons[j];
            new synapse(n,nn);
        }
    }
}

layer.prototype.reset = function () {
    for(var i =0; i < this.neurons.length;i++)
    {
        this.neurons[i].reset();
    }
}
module.exports = layer;