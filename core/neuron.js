/**
 * Created by root on 02.11.15.
 */

var synapse = require("./synapse.js");

var neuron = function (prevLayer,itsLayer,nextLayer,associatedValue) {
 this.inputs = []; //synapses from prev layer;
 this.outputs = []; //synapse to next layer;
 this.signal = 0;
 this.activationLevel = 1;
 this.associatedValue = associatedValue;
    if(itsLayer){
        itsLayer.neurons.push(this);
    }
    if(nextLayer)
    {
        for(var i = 0;i<nextLayer.neurons.length;i++) {
            new synapse(this,nextLayer.neurons[i]);
        }
    }
    if(prevLayer)
    {
        for(var i = 0;i<prevLayer.neurons.length;i++) {
            new synapse(prevLayer.neurons[i],this);
        }
    }

}

neuron.prototype.fire = function()
{
    // process signal

    var res = 0;
    if(this.signal>=this.activationLevel)
        res = this.signal;

    for(var i = 0 ; i<this.outputs.length;i++)
    {
        this.outputs[i].emit(res);
    }

}

neuron.prototype.collect= function(signal)
{
    this.signal += signal;
    this.fire();
}

neuron.prototype.setActivationLevel = function (level) {
    this.activationLevel = level;
}
neuron.prototype.reset = function () {
    this.signal = 0;
}

module.exports = neuron;