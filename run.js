
var fs = require('fs');
var http = require('http');

function onRequest(req,resp){

 fs.readFile('./game' + req.url, function(err, data) {
            if (err){
                resp.writeHead(503, {"Content-Type": "text/html"});
                resp.write(JSON.stringify(err));
                resp.end();
                return;
            }
            resp.writeHead(200, {"Content-Type": "text/html"});
            resp.write(data);
            resp.end();
        });
    }
        
http.createServer(onRequest).listen(process.env.OPENSHIFT_NODEJS_PORT||'80', process.env.OPENSHIFT_NODEJS_IP||'127.0.0.1');