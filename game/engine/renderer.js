﻿var tileWidth = 32;
var tileHeight = 32;

var sceneWidth = 20;
var sceneHeight = 20;

var spritesheet = new Image();
spritesheet.src = 'spritesheet.png';

var canvas, ctx, inventoryCanvas,mapCanvas,logContainer,mcCtx, mapCanvasData,invCtx;
function initRenderer(cnv,mapCnv,invCnv,logCont) {
	canvas = cnv;//document.getElementById('gameCanvas');
	if (!canvas) alert('Blah!');
	ctx = canvas.getContext("2d");
	if (!ctx) alert('Hmm!');
	canvas.width = sceneWidth * tileWidth;
	canvas.height = sceneHeight * tileHeight;

	mapCanvas = mapCnv;
	mapCanvas.width = 200;
	mapCanvas.height = 200;
	mcCtx = mapCanvas.getContext('2d');
	mapCanvasData = mcCtx.getImageData(0, 0, 200, 200);

	inventoryCanvas = invCnv;
	inventoryCanvas.width = 10*32;
	inventoryCanvas.height = 10*32;
	invCtx = inventoryCanvas.getContext('2d');

	logContainer = logCont;
}

var drawMapCD = 0;

function redraw(world) {

	//requestAnimationFrame(redraw);
	 
	//if (!spritesheetLoaded) return;
	
	if (drawMapCD <= 0) {
		setTimeout(function () { drawMap(world, mcCtx, 1, 1); }, 50);
		drawMapCD = 60;
	} else {
		drawMapCD--;
	}
	// clear the screen
	ctx.fillStyle = '#000000';
	ctx.fillRect(0, 0, canvas.width, canvas.height); 
	var vp = world.getViewPort(world.player.tile.x, world.player.tile.y, sceneWidth, sceneHeight);

	//calc player fog of war
	var pCoords = world.getPlayerCoordsInViewPort(world.player.tile.x, world.player.tile.y, sceneWidth, sceneHeight);

 var px = pCoords[0]*tileWidth + tileWidth/2,
		py = pCoords[1]*tileHeight+tileHeight/2;

	var segments = [];
	segments.push({a:{x:0,y:0},b:{x:0,y:sceneHeight*tileHeight}});
	segments.push({a:{x:0,y:sceneHeight*tileHeight},b:{x:sceneWidth*tileWidth,y:sceneHeight*tileHeight}});
	segments.push({a:{x:sceneWidth*tileWidth,y:sceneHeight*tileHeight},b:{x:sceneWidth*tileWidth,y:0}});
	segments.push({a:{x:sceneWidth*tileWidth,y:0},b:{x:0,y:0}});
	for (var x = 0; x < sceneWidth; x++) {
		for (var y = 0; y < sceneHeight; y++) {
			var tile = vp[x][y];
			var rx = tileWidth*x;
			var ry = tileHeight*y;
			if(tile.transparent) {
				if (tile.N && !tile.N.transparent)
					segments.push({a: {x: rx, y: ry}, b: {x: rx + tileWidth, y: ry}});
				if (tile.E && !tile.E.transparent)
					segments.push({a: {x: rx + tileWidth, y: ry}, b: {x: rx + tileWidth, y: ry + tileHeight}});
				if(tile.S&&!tile.S.transparent)
					segments.push({a:{x:rx+tileWidth,y:ry+tileHeight},b:{x:rx,y:ry+tileHeight}});
				if(tile.W&&!tile.W.transparent)
					segments.push({a:{x:rx,y:ry+tileHeight},b:{x:rx,y:ry}});
			}

		}
	}
	var polygons = [getSightPolygon(px,py,segments)];


	for(var i=1;i<polygons[0].length;i++){
		var p1 = polygons[0][i-1];
		var p2=polygons[0][i];


		var minX = Math.ceil(Math.min(p1.x,p2.x,px)*(1/tileWidth))-1;
		var minY = Math.ceil(Math.min(p1.y,p2.y,py)*(1/tileHeight))-1;
		var maxX = Math.floor(Math.max(p1.x,p2.x,px)*(1/tileWidth));
		var maxY = Math.floor(Math.max(p1.y,p2.y,py)*(1/tileHeight));

		if(maxX>sceneWidth-1)
			maxX=sceneWidth-1;
		if(maxY>sceneHeight-1)
			maxY=sceneHeight-1;
		if(minX<0)
			minX=0;
		if(minY<0)
			minY=0;
		for(var x=minX;x<=maxX;x++){
			for(var y = minY;y<=maxY;y++){
				 if(x<maxX/2&&y<maxY/2)
				 	continue;
				var visTile = vp[x][y];
				if(!visTile)
				console.log(x,y,maxX,maxY);
				visTile.isVisible = true;
				visTile.isRevealed = true;
			}
		}

	}

	//end calc fog
	for (var x = 0; x < sceneWidth; x++) {
		for (var y = 0; y < sceneHeight; y++) {
			var tile = vp[x][y];
			//tile.isChecked = false;
			if(!tile.isRevealed){
				tile.isVisible = false;
				continue;
			}

			for (var i = 0; i < tile.contents.length; i++) {
				var tileContent = tile.contents[i];
				// draw it
				// ctx.drawImage(img,sx,sy,swidth,sheight,x,y,width,height);
				if(!tile.isVisible&&tileContent instanceof Actor){
					continue;
				}
				ctx.drawImage(spritesheet,//img
			tileContent.tileIndex * tileWidth,// sx
			tileHeight * tileContent.tileLine || 0,//sy
			tileWidth, //sw 
			tileHeight,//sh
			x * tileWidth, //x
			y * tileHeight,//y
			tileWidth, //w
			tileHeight);//h
				
				if (tileContent.hp && tileContent.hp != tileContent.maxHp && tileContent.hp > 0) {
					
					//draw HP
					
					ctx.strokeStyle = "#df4b26";
					ctx.lineJoin = "round";
					ctx.lineWidth = 4;
					ctx.beginPath();
					ctx.moveTo(x * tileWidth, y * tileHeight);
					ctx.lineTo(x * tileWidth +
				((tileWidth * //15/100*15 
				(100 * tileContent.hp / tileContent.maxHp)) / 100),
				y * tileHeight);
					 
					ctx.stroke();
				}

				//draw fog of WAR
				
			}
			if(!tile.isVisible){
				ctx.fillStyle = 'rgba(0,0,8,0.6)';
				ctx.fillRect(x * tileWidth, y * tileHeight, tileWidth, tileHeight);
			}else{
			  var pdist = DiagonalDistance(tile, world.player.tile);
				 tile.distanceToPlayer = pdist;
				 var alpha = pdist / 15;
				 ctx.fillStyle = 'rgba(0,0,0,' + alpha.toFixed(2) + ')';
				 ctx.fillRect(x * tileWidth, y * tileHeight, tileWidth, tileHeight);
				tile.isVisible = false; 
			}

		//	delete tile.distanceToPlayer;
		}
	}

	drawInventory(invCtx,world.player.inventory);
	//writeLog();
		//player related drawings
	/*for (var i = 0; i < world.player.inventory.length; i++) {
			var item = world.player.inventory[i];
			ctx.drawImage(spritesheet,//img
			item.tileIndex * tileWidth,// sx
			tileHeight * item.tileLine || 0,//sy
			tileWidth, //sw 
			tileHeight,//sh
			i * tileWidth, //x
			sceneHeight * tileHeight,//y
			tileWidth, //w
			tileHeight);//h
		}*/
// Draw segments
/*	ctx.strokeStyle = "#999";
	for(var i=0;i<segments.length;i++){
		var seg = segments[i];
		ctx.beginPath();
		ctx.moveTo(seg.a.x,seg.a.y);
		ctx.lineTo(seg.b.x,seg.b.y);
		ctx.stroke();
	}
	*/
}


function getIntersection(ray,segment){

	// RAY in parametric: Point + Delta*T1
	var r_px = ray.a.x;
	var r_py = ray.a.y;
	var r_dx = ray.b.x-ray.a.x;
	var r_dy = ray.b.y-ray.a.y;

	// SEGMENT in parametric: Point + Delta*T2
	var s_px = segment.a.x;
	var s_py = segment.a.y;
	var s_dx = segment.b.x-segment.a.x;
	var s_dy = segment.b.y-segment.a.y;

	// Are they parallel? If so, no intersect
	var r_mag = Math.sqrt(r_dx*r_dx+r_dy*r_dy);
	var s_mag = Math.sqrt(s_dx*s_dx+s_dy*s_dy);
	if(r_dx/r_mag==s_dx/s_mag && r_dy/r_mag==s_dy/s_mag){
		// Unit vectors are the same.
		return null;
	}

	// SOLVE FOR T1 & T2
	// r_px+r_dx*T1 = s_px+s_dx*T2 && r_py+r_dy*T1 = s_py+s_dy*T2
	// ==> T1 = (s_px+s_dx*T2-r_px)/r_dx = (s_py+s_dy*T2-r_py)/r_dy
	// ==> s_px*r_dy + s_dx*T2*r_dy - r_px*r_dy = s_py*r_dx + s_dy*T2*r_dx - r_py*r_dx
	// ==> T2 = (r_dx*(s_py-r_py) + r_dy*(r_px-s_px))/(s_dx*r_dy - s_dy*r_dx)
	var T2 = (r_dx*(s_py-r_py) + r_dy*(r_px-s_px))/(s_dx*r_dy - s_dy*r_dx);
	var T1 = (s_px+s_dx*T2-r_px)/r_dx;

	// Must be within parametic whatevers for RAY/SEGMENT
	if(T1<0) return null;
	if(T2<0 || T2>1) return null;

	// Return the POINT OF INTERSECTION
	return {
		x: r_px+r_dx*T1,
		y: r_py+r_dy*T1,
		param: T1
	};

}



function getSightPolygon(sightX,sightY,segments){

	// Get all unique points
	var points = (function(segments){
		var a = [];
		for(var i =0;i<segments.length;i++)
		{
			var seg = segments[i];
			a.push(seg.a,seg.b);
		}

		return a;
	})(segments);
 
	var uniquePoints = (function(points){
		var set = {};
		return points.filter(function(p){
			var key = p.x+","+p.y;
			if(key in set){
				return false;
			}else{
				set[key]=true;
				return true;
			}
		});
	})(points);

	// Get all angles
	var uniqueAngles = [];
	for(var j=0;j<uniquePoints.length;j++){
		var uniquePoint = uniquePoints[j];
		var angle = Math.atan2(uniquePoint.y-sightY,uniquePoint.x-sightX);
		uniquePoint.angle = angle;
		uniqueAngles.push(angle-0.00001,angle,angle+0.00001);
	}

	// RAYS IN ALL DIRECTIONS
	var intersects = [];
	for(var j=0;j<uniqueAngles.length;j++){
		var angle = uniqueAngles[j];

		// Calculate dx & dy from angle
		var dx = Math.cos(angle);
		var dy = Math.sin(angle);

		// Ray from center of screen to mouse
		var ray = {
			a:{x:sightX,y:sightY},
			b:{x:sightX+dx,y:sightY+dy}
		};

		// Find CLOSEST intersection
		var closestIntersect = null;
		for(var i=0;i<segments.length;i++){
			var intersect = getIntersection(ray,segments[i]);
			if(!intersect||isNaN(intersect.x)||isNaN(intersect.y)) continue;
			if(!closestIntersect || intersect.param<closestIntersect.param){
				closestIntersect=intersect;
			}
		}

		// Intersect angle
		if(!closestIntersect||isNaN(closestIntersect.x)||isNaN(closestIntersect.y)) continue;
		closestIntersect.angle = angle;

		// Add to list of intersects
		intersects.push(closestIntersect);

	}

	// Sort intersects by angle
	intersects = intersects.sort(function(a,b){
		return a.angle-b.angle;
	});
	
/*	setTimeout(function () {
		ctx.strokeStyle = "#f55";
		for(var i=0;i<intersects.length;i++){
			var intersect = intersects[i];
			ctx.beginPath();
			ctx.moveTo(320+16,320+16);
			ctx.lineTo(intersect.x,intersect.y);
			ctx.stroke();
		}
	},10)*/

	// Polygon is intersects, in order of angle
	return intersects;

}

function drawPolygon(polygon,ctx,fillStyle){
	ctx.fillStyle = fillStyle;
	ctx.beginPath();
	ctx.moveTo(polygon[0].x,polygon[0].y);
	for(var i=1;i<polygon.length;i++){
		var intersect = polygon[i];
		ctx.lineTo(intersect.x,intersect.y);
	}
	ctx.fill();
}

function drawMap(world, ctx, tw, th) {
	
	ctx.fillStyle = '#000';
	ctx.fillRect(0, 0, world.map.length, world.map[0].length);
	for (var i = 0; i < world.map[0].length; i++) {
		var mapCol = world.map[0][i];
		for (var j = 0; j< mapCol.length; j++) {
			var t = mapCol[j];
			if (t > -1) {
				drawPixel(i * tw, j * th, 0, 125, 125,255, mapCanvasData);
			}
			  
		}
	}
	updateCanvas(mcCtx, mapCanvasData);


	ctx.fillStyle = '#f00';
	
//	ctx.fillRect(world.player.tile.x * tw, world.player.tile.y * th, tw, th); 
	var centerX = world.player.tile.x * tw;
	var centerY = world.player.tile.y * th;
	var radius = 5;
	
	ctx.beginPath();
	ctx.arc(centerX, centerY, radius, 0, 2 * Math.PI, false); 
	ctx.fill();
	 

}


// That's how you define the value of a pixel //
function drawPixel(x, y, r, g, b, a,canvasData) {
	var index = (x  + y *200) * 4;
	
	canvasData.data[index + 0] = r;
	canvasData.data[index + 1] = g;
	canvasData.data[index + 2] = b;
	canvasData.data[index + 3] = a;
}

// That's how you update the canvas, so that your //
// modification are taken in consideration //
function updateCanvas(_ctx,data) {
	_ctx.putImageData(data, 0, 0);
}


function drawInventory(_ctx,inventory){
	_ctx.fillStyle = '#000';
	_ctx.fillRect(0, 0, 10*tileWidth, 10*tileHeight);
	for(var i = 0;i<10;i++){
		for(var j=0;j<10;j++){
			var item = inventory[(j)+(i*10)];
			if(!item)
				return;
			_ctx.drawImage(spritesheet,//img
				item.tileIndex * tileWidth,// sx
				tileHeight * item.tileLine || 0,//sy
				tileWidth, //sw
				tileHeight,//sh
				j * tileWidth, //x
				i * tileHeight,//y
				tileWidth, //w
				tileHeight);//h
		}
	}
}

function writeLog(){
	if(!logContainer)
		return;
	for(var i in world.player.messages)
	{
		var m =world.player.messages[i];

		if(logContainer.innerHTML.length>1000){
			logContainer.innerHTML = logContainer.innerHTML.substring(0,500);
		}
		logContainer.innerHTML = m + '<br/>' + logContainer.innerHTML;
	}
	world.player.messages = [];
}