﻿function extend(child, parent) {
	child.prototype = Object.create(parent.prototype);
	child.parent = parent;
	 
}

function getRandomInt(min, max) {
	
	return Math.floor(Math.random() * (max - min + 1)) + min;
}


var abs = Math.abs;
var max = Math.max;
var pow = Math.pow;
var sqrt = Math.sqrt;
// distanceFunction functions
// these return how far away a point is to another

function ManhattanDistance(Point, Goal) {	// linear movement - no diagonals - just cardinal directions (NSEW)
	return abs(Point.x - Goal.x) + abs(Point.y - Goal.y);
}

function DiagonalDistance(Point, Goal) {	// diagonal movement - assumes diag dist is 1, same as cardinals
	return max(abs(Point.x - Goal.x), abs(Point.y - Goal.y));
}

function EuclideanDistance(Point, Goal) {	// diagonals are considered a little farther than cardinal directions
	// diagonal movement using Euclide (AC = sqrt(AB^2 + BC^2))
	// where AB = x2 - x1 and BC = y2 - y1 and AC will be [x3, y3]
	return parseFloat(sqrt(pow(Point.x - Goal.x , 2) + pow(Point.y - Goal.y, 2)).toFixed(1));
}