﻿
var keydown, canvasClick,invClick;
 
function setUpControls(canvas,world,inv) {
	window.addEventListener('keydown', keydown = function (e) {
		clearRecursiveInterval();
		console.log(e.keyCode);
		switch (e.keyCode) {
			case 37:
				world.userInput('W');
				break;
			case 38:
				world.userInput('N');
				break;
			case 39:
				world.userInput('E');
				break;
			case 40:
				world.userInput('S');
				break;
			case 33:
				world.userInput('NE');
				break;
			case 34:
				world.userInput('SE');
				break;
			case 35:
				world.userInput('SW');
				break;
			case 36:
				world.userInput('NW');
				break;
			default:
				
				break;
		}
	});
	
	canvas.addEventListener('click', canvasClick = function (e) {
		clearRecursiveInterval();
		var x;
		var y;
		
		// grab html page coords
		if (e.pageX != undefined && e.pageY != undefined) {
			x = e.pageX;
			y = e.pageY;
		}
		else {
			x = e.clientX + document.body.scrollLeft +
		document.documentElement.scrollLeft;
			y = e.clientY + document.body.scrollTop +
		document.documentElement.scrollTop;
		}
		
		// make them relative to the canvas only
		x -= canvas.offsetLeft;
		y -= canvas.offsetTop;
		var p = world.player;
		// return tile x,y that we clicked
		var cell =
		[
			Math.floor(x / tileWidth),
			Math.floor(y / tileHeight)
		];

			
			var pCoords = world.getPlayerCoordsInViewPort(p.tile.x, p.tile.y, 20, 20);
			var absCellCoords = [p.tile.x + cell[0] - pCoords[0], p.tile.y + cell[1] - pCoords[1]];
			var path = findPath(world.plain, [p.tile.x, p.tile.y], 
				absCellCoords, p,true,true);
			
			console.log(path.length);
			if (path.length <= 2) {
				world.userInput(absCellCoords, function () { });
			} else {
				
				var wasInterrupted = false;
				recursiveInterval(function (args, i) {
					if (typeof i == 'undefined')
						i =0;
					
					if (!wasInterrupted&&i>0)
						world.userInput([path[i][0], path[i][1]], function () { wasInterrupted = true; });
				}, path, 50, path.length);
			}

		console.log(cell)
	});

	inv.addEventListener('click',invClick = function (e) {
		var x;
		var y;
		var p = world.player;
		// grab html page coords
		if (e.pageX != undefined && e.pageY != undefined) {
			x = e.pageX;
			y = e.pageY;
		}
		else {
			x = e.clientX + document.body.scrollLeft +
				document.documentElement.scrollLeft;
			y = e.clientY + document.body.scrollTop +
				document.documentElement.scrollTop;
		}

		// make them relative to the canvas only
		x -= inv.offsetLeft;
		y -= inv.offsetTop;
		var cell =
			[
				Math.floor(x / tileWidth),
				Math.floor(y / tileHeight)
			];

			var i = world.player.inventory[cell[0]+cell[1]*10];
			if (i)
				i.use(p, p);

		console.log(cell)

	});
}
var latestTimeout = 0;
function recursiveInterval(fx, args, timeout, maxIter,i) {
	if (typeof i == 'undefined')
		i = 0;
	if (typeof maxIter != 'undefined' && maxIter <=i)
		return;
	 
	latestTimeout = setTimeout(function () {
		fx(args, i);
		 
		recursiveInterval(fx,args, timeout, maxIter,++i);
	}, timeout);
 
}

function clearRecursiveInterval() {
	if(latestTimeout)
		clearTimeout(latestTimeout);
	clearTimeout(latestTimeout+1);
	 
}

function removeListeners() {
	window.removeEventListener('keydown',keydown);
	canvas.removeEventListener('click',canvasClick);
	inventoryCanvas.removeEventListener('click',invClick);
}