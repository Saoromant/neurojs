var Tile = function (x, y, plain, world) {
    this.world = world;
    this.contents = [];
    this.x = x;
    this.y = y;
    this.transparent = 1;
    this.isRevealed = false;
    this.isVisible = false;
    this.putObject = function (object) {
        this.contents.push(object);
        if (typeof(object.transparent) != 'undefined') {
            this.transparent &= object.transparent;
        }
        object.tile = this;
        if (!object.world) {
            object.world = this.world;
            if (object instanceof Actor)
                this.world.actors.push(object);
        }
    }
    this.removeObject = function (object) {
        if (typeof(object.transparent) != 'undefined') {
            this.transparent = true;
            for (var i = 0; i < this.contents.length; i++) {
                var tc = this.contents[i];
                if (typeof(tc.transparent) != 'undefined')
                    this.transparent &= tc.transparent;
            }

        }
        var objIndex = this.contents.indexOf(object);
        this.contents.splice(objIndex, 1);
        object.tile = null;
    }

}


var World = function (x, y) {
    this.player = new ActiveObjects.player();
    this.actors = [];
    this.width = x;
    this.height = y;
    this.plain = [];
    var map = this.map = generateMap(x, x, y);

    var tileMap = map[0];
    var rooms = map[1];

    for (var i = 0; i < x; i++) {
        var pi = this.plain[i] = [];
        for (var j = 0; j < y; j++) {
            var tile = new Tile(i, j, this.plain, this);
            pi[j] = tile;
            var rnd2 = Math.random();
            if (tileMap[i][j] == -1) {
                tile.putObject((rnd2 > 0.65 ? StaticObjects.stone1 : StaticObjects.stone));
            }
            else {
                tile.putObject((rnd2 > 0.99 ? StaticObjects.grass1 : StaticObjects.grass));
            }

            if (i != 0) {
                tile.W = this.plain[i - 1][j];
                this.plain[i - 1][j].E = tile;
            }
            if (j != 0) {
                tile.N = this.plain[i][j - 1];
                this.plain[i][j - 1].S = tile;

            }
            if (i != 0 && j != 0) {
                tile.NW = this.plain[i - 1][j - 1];
                this.plain[i - 1][j - 1].SE = tile;
            }
            if (j < y - 1 && i != 0) {

                tile.SW = this.plain[i - 1][j + 1];
                this.plain[i - 1][j + 1].NE = tile;

            }


        }
    }

    var playerPlaced = false;
    for (var i = 0; i < rooms.length; i++) {
        var room = rooms[i];
        if (!room.placed)
            continue;

        var coords = room.getRandomCoords();
        var tile = this.plain[coords[0]][coords[1]];
        if (!playerPlaced) {
            tile.putObject(this.player);
            playerPlaced = true;
            continue;
        }

        var mNumber = getRandomInt(0, 4);
        while (mNumber > 0) {
            var wr = new ActiveObjects.skeleton(tile);
            tile.putObject(wr);
            mNumber--;
        }

        mNumber = getRandomInt(0, 1);
        while (mNumber > 0) {
            var wr = new ActiveObjects.wraith(tile);
            tile.putObject(wr);
            mNumber--;
        }

        if (room.sizeX > 10 && room.sizeY > 10)
            tile.putObject(new ActiveObjects.broodMother())

    }
}
World.prototype.moveObject = function (object, toTile) {
    if (!toTile) {
        return false;
    }
    for (var i = 0; i < toTile.contents.length; i++) {
        var c = toTile.contents[i];
        if (typeof c.maxSizePass != 'undefined')
            if (object.size > c.maxSizePass)
                return false;
    }
    object.tile.removeObject(object);
    toTile.putObject(object);
    return true;
}
World.prototype.removeActor = function (ao) {
    var ind = this.actors.indexOf(ao);
    this.actors.splice(ind, 1);
}

World.prototype.getViewPortCoords = function (x, y, width, height) {
    var x0 = x - width / 2;
    var y0 = y - height / 2;
    if ((x0 + width) > this.width)
        x0 = this.width - width;
    else if (x0 < 0)
        x0 = 0;
    if ((y0 + height) > this.height)
        y0 = this.height - height;
    if (y0 < 0)
        y0 = 0;
    return [x0, y0];
}

World.prototype.getViewPort = function (x0, y0, width, height) {
    var res = [];
    var coords = this.getViewPortCoords(x0, y0, width, height);
    for (var i = 0; i < width; i++) {
        res[i] = [];
        for (var j = 0; j < height; j++) {
            res[i][j] = this.plain[coords[0] + i][coords[1] + j];
        }
    }
    return res;
}


World.prototype.getPlayerCoordsInViewPort = function (x, y, width, height) {
    var x0 = width / 2;
    var y0 = height / 2;
    if ((x - x0 + width) > this.width) //close to right world wall
        x0 = width - (this.width - x);
    else if (x - x0 < 0) //close to left wall
        x0 = x;
    if ((y - y0 + height) > this.height) //close to south wall
        y0 = height - (this.height - y);
    if (y - y0 < 0) //close to north wall
        y0 = y;
    return [x0, y0];
}


World.prototype.proceedTurn = function (args) {
    for (var i = 0; i < this.actors.length; i++) {
        var act = this.actors[i];
        if (act.proceedTurn)
            act.proceedTurn(args);
    }
    redraw(this);
}

World.prototype.userInput = function (args, onInterrupt) {
    if (onInterrupt)
        this.player.onInterrupt = onInterrupt;
    for (var i = this.player.speed; i > 0; i--)
        this.proceedTurn(args);
}

World.prototype.toJSON = function () {
var obj = {
    tiles:[]
};
    obj.w = this.width;
    obj.h = this.height;
    for(var i = 0;i<this.width;i++){
        for(var j =0; j<this.height;j++){

        }
    }

    return JSON.stringify(obj);

}



if (typeof module != 'undefined')
    module.exports = World;
 