﻿
var distanceFunc = EuclideanDistance;
function generateMap(rn,x,y) {
	var map = [];
	var rooms = [];
	var totalX = 0;
	var totalY = 0;
	for (var i = 0; i < x; i++) {
		map[i] = [];
		for (var j = 0; j < y; j++) {
			map[i][j] = -1; //wall
		}
	}

	for (var i = 0; i < rn ; i++) {
		var rx = getRandomInt(6, 14);
		totalX += rx;
		var ry = getRandomInt(6, 14);
		totalY += ry;
		generateSquareRoom(rx, ry, rooms);
	}
 
	for (var i = 0; i < rooms.length; i++) {
		placeRoom(rooms[i], map, 0);
	}
 

	for (var r in rooms) {
		var room = rooms[r];
		if (!room.placed)
			continue;
		var connections = getRandomInt(1,3);
		if (!room.conneted)
			room.connected = [];
		while (connections > 0) {
			var closest = findClosestRoom(room, rooms, room.connected);
			if (!closest.placed)
				continue;
			room.connected.push(closest);
			if (!closest.connected)
				closest.connected = [];
			closest.connected.push(room);
			var corridor = findPath(map, 
			[Math.floor(room.x + room.sizeX / 2), Math.floor(room.y + room.sizeY / 2)], 
			[Math.floor(closest.x + closest.sizeX / 2), Math.floor(closest.y + closest.sizeY / 2)],null,false,false);
			
			
			for (var p = 0; p < corridor.length; p++) {
				var pathPoint = corridor[p];
				map[pathPoint[0]][pathPoint[1]] = 0;
			}
			connections--;
		}
	}

	return [map,rooms];
}


function generateSquareRoom(sizeX,sizeY,rooms) {
	var room = {
		sizeX: sizeX,
		sizeY: sizeY,
		getRandomCoords : function () { 
			return [getRandomInt(this.x,this.x+this.sizeX-1),getRandomInt(this.y, this.y + this.sizeY-1)]
		}
	};
	room.id = rooms.push(room) -1;
 
}

function placeRoom(room, map,iter) {
	if (!iter)
		iter = 1;
	else
		iter++;
	var rndX = getRandomInt(1, map.length);
	var rndY = getRandomInt(1, map[0].length);
	

	
	var check = true;
	for (var i = rndX-1; i < (rndX + room.sizeX); i++) {
		var col = map[i];
		if (!col) {
			check = false;
			break;
		}
		for (var j = rndY-1; j < (rndY + room.sizeY); j++) {
			var point = col[j];
			if (!point || point != -1) {
				check = false;
				break;
			}
		}
	}

	if (check) { 
		for (var i = rndX; i < (rndX + room.sizeX); i++) {
			for (var j = rndY; j < (rndY + room.sizeY); j++) {
				room.placed = true;
				room.x = rndX;
				room.y = rndY;
				map[i][j] = room.id; 
			}
		}
		return;
	}

	if (iter < 10)
		return;

	placeRoom(room, map, iter);
}

function findClosestRoom(room,rooms,ignore) {
 
		var mid = {
			x: room.x + (room.sizeX / 2),
			y: room.y + (room.sizeY / 2)
		};
		var closest = null;
		var closest_distance = 1000;
		for (var i = 0; i < rooms.length; i++) {
			var check = rooms[i];
			if (check == room||(ignore&& ignore.indexOf(check)!=-1) || !check.placed) continue;
			var check_mid = {
				x: check.x + (check.sizeX / 2),
				y: check.y + (check.sizeY / 2)
			};
		var distance = distanceFunc(check, room);
			if (distance < closest_distance) {
				closest_distance = distance;
				closest = check;
			}
		}
		return closest;
	} 

