﻿var StaticObjects = {};



function checkTilePass(tile,object) {
 
	
	for (var i = 0; i < tile.contents.length; i++) {
		var tc = tile.contents[i];
		if (typeof tc.maxSizePass !='undefined' && tc.maxSizePass < object.size) {
			
			return false
		}
				
	}
	return true;
}

var TileContent = function ( ) {
 
}
var Actor = function () { 
	Actor.parent.apply(this, arguments);
	this.turnCooldown = 0;
	this.speed = 1;
	this.hp = 1;
	this.maxHp = 1;
	this.accuracy = 0.8; //1-always true
	this.evasion = 0.0; // 1 - always evading
	this.protection = 1; //base protection, higher - better
}
extend(Actor, TileContent);
Actor.prototype.makeTurn = function () { }
Actor.prototype.proceedTurn = function (args) { 
	if (this.hp <= 0)
		this.die();
	else if (this.turnCooldown == 0) {
		this.makeTurn(args);
		this.turnCooldown = this.speed;
	}
	this.turnCooldown--;
}
Actor.prototype.die = function () {
	this.tileLine = 1;
	this.world.removeActor(this);
	this.isEnemy = 0;
	delete this.maxSizePass;
	while (this.inventory.length > 0) {
		this.tile.putObject(this.inventory.pop());
	}
	 
	return;
}

Actor.prototype.attack = function (enemy) {
	var rawDmg = this.dmg;
	var dd =enemy.recieveAttack(this, rawDmg);
	if(this.log)
		this.log('You hit an enemy for '+dd);
}
Actor.prototype.recieveAttack = function (enemy,dmg) {
	 
	if (enemy.accuracy < Math.random()) {
		if(this.log)
			this.log('An enemy attacks you, but missed');
		return 0;
	}
	if (this.evasion > Math.random()) {
		if(this.log)
			this.log('An enemy attacks you, but you dodged');
		return 0;
	}
	var pure = Math.round(dmg / this.protection);
	this.hp -= pure;
	if (this.hp <= 0) {
		this.die();
		enemy.exp += this.maxHp;
	}
	if(this.log)
		this.log('An enemy hit you for '+pure);
	return pure;
}

var Enemy = function () {
	this.isEnemy = 1;
	Enemy.parent.apply(this, arguments);
} 
extend(Enemy, Actor);
Enemy.prototype.makeTurn = function () {
	 
	var actor = this.world.player;
	var dist = ManhattanDistance(this.tile, actor.tile);
	if (dist < this.seePlayerAtDistance && dist > 1) {
		
		var path = findPath(this.tile.world.plain,
				[this.tile.x, this.tile.y],
				[actor.tile.x, actor.tile.y], this,true,true);
		
		if (path.length > 0)
			this.tile.world.moveObject(this, this.world.plain[path[1][0]][path[1][1]])
	} else if (dist == 1) {
		this.attack(actor);
	} else {
		var rnd = Math.random();
		if (rnd < 0.25) {
			this.tile.world.moveObject(this, this.tile.N)
		}
		else if (rnd >= 0.25 && rnd < 0.5) {
			this.tile.world.moveObject(this, this.tile.E)
		}
		else if (rnd >= 0.5 && rnd < 0.75) {
			this.tile.world.moveObject(this, this.tile.S)
		}
		else if (rnd >= 0.75 && rnd < 1) {
			this.tile.world.moveObject(this, this.tile.W)
		}
	}
}
StaticObjects.grass = 
{
	tileIndex: 0,
	renderPriority: 0,
	transparent:1
}

StaticObjects.grass1 = 
{
	tileIndex: 0, 
	tileLine: 1, //->1
	transparent:1
}

StaticObjects.stone = 
{
	tileIndex: 1, 
	size : 100500, 
	maxSizePass: 0,
	transparent:0
}
StaticObjects.stone1 = 
{
	tileIndex: 1, 
	size : 100500,
	maxSizePass: 0,
	tileLine: 1,
	transparent:0
};

StaticObjects.portal =
{
	tileIndex:0,
	tileLine:1,
	size : 100500,
	transparent:0,
	interact: function (actor) {
		this.world.moveToNextLocation(actor);
	}
}


ActiveObjects = {};
ActiveObjects.skeleton = function () {
	ActiveObjects.skeleton.parent.apply(this, arguments);
	this.maxSizePass = 1;
	this.maxHp = 15;
	this.tileIndex = 3;
	this.speed = 5;
	this.size = 2;
	this.hp = 15;
	this.dmg = 3;
	this.inventory = [];
	this.seePlayerAtDistance = 10;
	if (Math.random() > 0.50)
		this.inventory.push(new ActiveObjects.healingPotion());
	
}
extend(ActiveObjects.skeleton, Enemy);

ActiveObjects.skeleton.prototype.die = function () {
	ActiveObjects.skeleton.parent.prototype.die.apply(this, arguments);
	if (Math.random() > 0.65)
		this.tile.putObject(new ActiveObjects.wraith());
}

ActiveObjects.wraith = function () {
	ActiveObjects.wraith.parent.apply(this, arguments);
	this.maxSizePass = 1;
	this.maxHp = 3;
	this.tileIndex = 4;
	this.size = 0;
	this.hp = 3;
	this.dmg = 5;
	this.speed = 2;
	this.evasion = 0.45;
	this.accuracy = 0.6;
	this.inventory = [];
	this.seePlayerAtDistance = 20;
	if (Math.random() > 0.20)
		this.inventory.push(new ActiveObjects.healingPotion()); 
}
extend(ActiveObjects.wraith, Enemy);


ActiveObjects.broodMother = function () {
	ActiveObjects.broodMother.parent.apply(this, arguments);
	this.maxSizePass = 1;
	this.maxHp = 100;
	this.tileIndex = 5;
	this.size = 2;
	this.hp = 100;
	this.dmg = 0;
	this.speed = 150;
	this.evasion = 0;
	this.inventory = [];
	if (Math.random() > 0.65)
		this.inventory.push(new ActiveObjects.strengthPotion());
	this.seePlayerAtDistance = 20;
	//if (Math.random() > 0.20)
	//	this.inventory.push(new ActiveObjects.healingPotion());
}
extend(ActiveObjects.broodMother, Enemy);

ActiveObjects.broodMother.prototype.makeTurn = function () {
	console.log('breeding')
	var t;
	var tile = this.tile;
 
	this.hp += Math.min((this.maxHp * 5 / 100), this.maxHp - this.hp);
	var rnd = getRandomInt(0, 4);
 
		switch (rnd) {
			case 0:
				t = tile.N;
				break;
			case 1:
				t = tile.W;
				break;
			case 2:
				t = tile.S;
				break;
			case 3:
				t = tile.E;
				break;
			default:
				t = tile.N || tile.E || tile.S || tile.W;
				break;
		}
		if (t) {
			var nbm = (rnd>2?new ActiveObjects.broodMother():new ActiveObjects.broodling())
			nbm.turnCooldown = this.speed * 2;
			if (checkTilePass(t, nbm)) {
				t.putObject(nbm);
				 
			}
		}
	 
}


ActiveObjects.broodling = function () {
	ActiveObjects.broodling.parent.apply(this, arguments);
	this.maxSizePass = 1;
	this.maxHp = 1;
	this.tileIndex = 6;
	this.size = 1;
	this.hp = 1;
	this.dmg = 1;
	this.speed = 1;
	this.evasion = 0.3;
	this.accuracy = 0.5;
	this.inventory = [];
	this.seePlayerAtDistance = 5;
	//if (Math.random() > 0.20)
	//	this.inventory.push(new ActiveObjects.healingPotion());
}
extend(ActiveObjects.broodling, Enemy);


var Item = function () {
	Item.parent.apply(this, arguments);
	this.isItem = 1;
}
extend(Item, TileContent);

Item.prototype.use = function (user, target) {
	
}

ActiveObjects.healingPotion = function () {
	ActiveObjects.healingPotion.parent.apply(this, arguments);
	this.hp = 25;
	this.tileIndex = 0;
	this.tileLine = 2; 
}
extend(ActiveObjects.healingPotion, Item);
ActiveObjects.healingPotion.prototype.use = function (user, target) {
	ActiveObjects.healingPotion.parent.prototype.use.apply(this, arguments);

	target.hp += Math.min(this.hp, target.maxHp - target.hp);
	user.inventory.splice(user.inventory.indexOf(this), 1);
	redraw(world);
}

ActiveObjects.strengthPotion = function () {
	this.dmg = 1;
	this.maxHp = 2;
	this.hp  = 2;
	this.tileIndex =1;
	this.tileLine = 2;
	this.isItem = 1;
}
extend(ActiveObjects.strengthPotion, Item);
ActiveObjects.strengthPotion.prototype.use = function (user, target) {
	ActiveObjects.strengthPotion.parent.prototype.use.apply(this, arguments);
	
	target.dmg += this.dmg;
	target.hp += this.hp;
	target.maxHp += this.maxHp;
	user.inventory.splice(user.inventory.indexOf(this), 1);
	redraw(world);
}
ActiveObjects.player = function (log) {
	ActiveObjects.player.parent.apply(this, arguments);
	this.inventory = [];
	this.tileIndex = 2;
	this.maxHp = 100;
	this.size = 2;
	this.maxSizePass = 1;
	this.hp = 100;
	this.dmg = 5;
	this.speed = 3;
	this.exp = 0;
	this.levelAt = 100;
	this.messages = [];
	this.log =  function (message) {
			this.messages.push(message);
		};



 }
extend(ActiveObjects.player, Actor);

ActiveObjects.player.prototype.recieveAttack = function () {
	ActiveObjects.player.parent.prototype.recieveAttack.apply(this, arguments);
	if (this.onInterrupt)
		this.onInterrupt();
}
ActiveObjects.player.prototype.makeTurn = function (args) {
	
	if (this.exp >= this.levelAt) {
		this.exp -= this.levelAt;
		this.levelAt *= 3;
		this.maxHp += 15;
		this.hp += 15;
		this.protection += 0.1;
		this.accuracy += 0.01;
		this.evasion += 0.01;
		this.dmg +=0.5;
		this.log('Level '+ (Math.log(this.levelAt/100)/Math.log(3))  + ' reached!');
	}
	var targetTile;
	if (typeof args == 'string')
		targetTile = this.tile[args];	
		else
	 targetTile = this.world.plain[args[0]][args[1]]; 
	if (!targetTile) {
		if (this.onInterrupt)
			this.onInterrupt();
		return;
	
	}
	
	for (var i = 0; i < targetTile.contents.length; i++) {
		var tc = targetTile.contents[i];
		if (tc.isItem) {
			targetTile.removeObject(tc);
			this.inventory.push(tc);
		}  
		if (tc.isEnemy && tc.hp) {
			if (this.onInterrupt)
				this.onInterrupt(tc);
			this.attack(tc);
			return;
		}
	}
	
	this.world.moveObject(this, targetTile);

}
