function sendAJAX(url, data, callback) {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open("POST", url);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.setRequestHeader("Origin", "localhost");
    var stringData = "";
    var keys = Object.keys(data);
    for (var i = 0; i < keys.length; i++) {
        var k = keys[i];
        if (stringData)
            stringData += '&';
        stringData += k + "=" + data[k];
    }

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4) {
           // if (xmlhttp.status == 400) {
            console.log(xmlhttp.response)
                if (callback)
                    callback(xmlhttp.responseText);
       //     } else {
       //         console.log('error')
       //     }
        }
    }
    xmlhttp.send(stringData);
}

var neuralData = [];
function getJson() {

    var json = JSON.stringify(neuralData);


    return json;
}

function askNet() {
    sendAJAX('http://localhost/ask', {data: getJson()}, function (resp) {
        document.getElementById('jcont').innerHTML = resp;
    });
}

function teachNet() {
    var json = getJson();
    var associatedVal = document.getElementById('inpData').value;
    sendAJAX('http://localhost/teach', {data: json, val: associatedVal});
}
function main() {

    var element = document.getElementById("canv");
    var context = element.getContext("2d");

    for (var w = 0; w < element.width; w++) {
        neuralData[w] = [];
        for (var h = 0; h < element.height; h++) {
            neuralData[w][h] = 0;
        }
    }


    var isPainting = false;
    var prevCoords;

    element.onmousedown = onMouseClick;
    element.onmousemove = mouseMove;
    element.onmouseout = element.onmouseup = endPaint;

    function onMouseClick(event) {

        var coords = calcCoords(event, this);

        beginPaint();

        prevCoords = coords;
    }

    function calcCoords(event, element) {

        return [
            event.pageX - element.offsetLeft,
            event.pageY - element.offsetTop
        ];
    }

    function mouseMove(e) {

        if (!isPainting)
            return;

        var coords = calcCoords(e, this);
        context.moveTo(prevCoords[0], prevCoords[1]);
        context.lineTo(coords[0], coords[1]);
        prevCoords = coords;
        context.closePath();
        context.stroke();
        neuralData[coords[0]][coords[1]] = 1;
    }

    function endPaint(e) {
        isPainting = false;

    }

    function beginPaint() {
        if (isPainting)
            return;
        isPainting = true;
        context.strokeStyle = "#df4b26";
        context.lineJoin = "round";
        context.lineWidth = 1;
        context.beginPath();
    }


    function addClick(coords) {
        clicks.push(coords);

    }

    window.clearCanv = function cls() {
        context.clearRect(0, 0, context.canvas.width, context.canvas.height);
        context.lineJoin = "round";
        context.lineWidth = 1;
        for (var w = 0; w < element.width; w++) {
            neuralData[w] = [];
            for (var h = 0; h < element.height; h++) {
                neuralData[w][h] = 0;
            }
        }
    }

}

document.addEventListener("DOMContentLoaded", function (event) {
    main();
});
