/**
 * Created by root on 02.11.15.
 */
var network = require("./core/network.js");
var http = require('http');
var url = require('url');
var fs = require('fs');
var qs = require('querystring');
var Q = require('q');

function readData (request, response) {
    var defer  = Q.defer();
    if (request.method == 'POST') {
        var body = '';

        request.on('data', function (data) {
            body += data;
            // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
            if (body.length > 1e6){
                defer.reject('Too much POST data, kill the connection!');
                request.connection.destroy();
            }
        });

        request.on('end', function () {
            var post = qs.parse(body);
            defer.resolve(post);
        });
    }
    return defer.promise;
}

function cliNeuralDataToFlatArray(raw)
{
    var parsed = JSON.parse(raw);
    var res = [];
    for(var i = 0;i<parsed.length;i++)
    {
        for(var j =0;j<parsed[i].length;j++)
        {
            res.push(parsed[i][j]);
        }
    }
    return res;
}

var n = new network();
console.log('net created');

function onRequest(req,resp)
{
    resp.writeHeader(200,{'Access-Control-Allow-Origin':'*'});
    resp.writeHeader(200, {"Content-Type": "text/html"});
    var _u = url.parse(req.url, true, true);
    if(_u.path.indexOf('/teach') == 0){
        readData(req,resp)
            .then(function (post) {

                n.learn(
                    cliNeuralDataToFlatArray(post.data)
                    ,post.val);

                resp.end();
            });
    }
    else if(_u.path.indexOf('/ask') == 0){
        readData(req,resp)
            .then(function (post) {
                var answer = n.test(
                    cliNeuralDataToFlatArray(post.data)
                );
                resp.write(answer);
                resp.end();
            });
    }
    else
    {

        fs.readFile('./' + req.url, function(err, data) {
            if (err){
                resp.writeHeader(503, {"Content-Type": "text/html"});
                resp.write(JSON.stringify(err));
                resp.end();
            }
            resp.writeHeader(200, {"Content-Type": "text/html"});
            resp.write(data);
            resp.end();
        });


    }

}
http.createServer(onRequest).listen(process.env.OPENSHIFT_NODEJS_PORT||'8080', process.env.OPENSHIFT_NODEJS_IP||'127.0.0.1');